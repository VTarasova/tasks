**First tasks:**

**Create "To do" Web application following the requirements:**

1. Main model - "Employee": name, age, speciality, employement date, list of tasks, etc.
2. Implement CRUD for the models.
3. Implement authentication through a separate REST service (use signed and encrypted JWT) that must be independent.
4. App should get data directly from db.
5. Build scalable, flexible and adaptive UI.
6. Add flexible search of employees.
7. Add a possibility to create some tasks for employees.
8. Add flexible search for tasks.
9. Implement avatars for employees and display them in the search page.
10. Make sure the app is optimized.
11. It mustn't be fallen - user needs to get understandable error messages.
12. Implement logging with a suitable logging strategy. Logs must be useful.

**To the first application:**
1.Clone project
2. Build solution
3. Pablish datebase project on Server=(localdb)\\MSSQLLocalDB;
4. DB name = EmployeeManagement
5. Login in application(or registration): 
    Admin: 
        email: petrov@gmail.com 
        password: 1213qazxsw5445
    User:
        email: durov@gmail.com
        password: 1213qazxsw5445
    If your role is Admin you can:
        1. Change your account
        2. Change the specialty of other employees
        3. Create task
        4. Add tasks to an employee
        5. Search task or employee
    If your role is User:
        1. Change your account
        2. View a list of your tasks
        3. Search task

    

